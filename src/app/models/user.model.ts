export class UserModel {
    constructor(
        public Id?: number,
        public FullName?: string,
        public Email?: string,
        public Username?: string,
        public Password?: string,
        public Role?: number,
        public Status?: number,
        public Errors?: Array<string>) { }
}