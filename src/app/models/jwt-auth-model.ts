export class JWTAuthModel {
    public accesExpires? : Date = new Date();

    constructor(
        public accesToken?: string,
        public refreshToken?: string) { }
}