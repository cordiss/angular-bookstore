import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersListComponent } from 'src/app/components/user/users-list/users-list.component';
import { UserProfileComponent } from 'src/app/components/user/user-profile/user-profile.component';


const routes: Routes = [
  {path: 'users', component: UsersListComponent},
  {path: 'profile', component: UserProfileComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
