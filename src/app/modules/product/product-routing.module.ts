import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductsListComponent } from 'src/app/components/products/products-list/products-list.component';
import { ProductsHomeComponent } from 'src/app/components/products/products-home/products-home.component';
import { ProductsDetailsComponent } from 'src/app/components/products/products-details/products-details.component';


const routes: Routes = [
  {path: 'home', component: ProductsHomeComponent},
  {path: 'products', component: ProductsListComponent},
  {path: 'home/product/:id', component:ProductsDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
