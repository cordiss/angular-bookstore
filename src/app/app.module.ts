import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserModule } from './modules/user/user.module';
import { AccountModule } from './modules/account/account.module';
import { ProductModule } from './modules/product/product.module';
import { OrderModule } from './modules/order/order.module';
import { CartModule } from './modules/cart/cart.module';
import { AuthorModule } from './modules/author/author.module';
import { SignInComponent } from './components/account/sign-in/sign-in.component';
import { SignUpComponent } from './components/account/sign-up/sign-up.component';
import { AuthorComponent } from './components/author/author.component';
import { CartComponent } from './components/cart/cart/cart.component';
import { OrderComponent } from './components/order/order.component';
import { ProductsDetailsComponent } from './components/products/products-details/products-details.component';
import { ProductsHomeComponent } from './components/products/products-home/products-home.component';
import { ProductsListComponent } from './components/products/products-list/products-list.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { UserProfileComponent } from './components/user/user-profile/user-profile.component';
import { UsersListComponent } from './components/user/users-list/users-list.component';

@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    SignUpComponent,
    AuthorComponent,
    CartComponent,
    OrderComponent,
    ProductsDetailsComponent,
    ProductsHomeComponent,
    ProductsListComponent,
    HeaderComponent,
    FooterComponent,
    UserProfileComponent,
    UsersListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    UserModule,
    AccountModule,
    ProductModule,
    OrderModule,
    CartModule,
    AuthorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
