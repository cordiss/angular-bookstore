import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { UserAccountModel } from '../models/login-model';
import { UserModel } from '../models/user.model';
import { JWTAuthModel } from '../models/jwt-auth-model';
import { BehaviorSubject, Observable } from 'rxjs';

import { map } from 'rxjs/operators';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

@Injectable()
export class AccountService { 
    private url = "https://localhost:5001/api/Account";
    private currentUserSubject: BehaviorSubject<UserModel>;
    public currentUser: Observable<UserModel>;

    constructor(private http: HttpClient) {
      this.currentUserSubject = new BehaviorSubject<UserModel>(JSON.parse(localStorage.getItem('currentUser')));
     }
 
    SignIn(login: UserAccountModel) {
        const body = {email: login.email, password: login.password};

        return this.http.post<any>(this.url, JSON.stringify(body), httpOptions).pipe(map(user => {
          localStorage.setItem('currentUser', JSON.stringify(body));
          this.currentUserSubject.next(user);
          return user;
      }));
    }

    CreateAccount(user: UserModel) {
      const body = {FullName: user.FullName, Email: user.Email, UserName: user.Email, Password: user.Password};

      this.http.post("https://localhost:5001/api/User/CreateAccount", JSON.stringify(body), httpOptions).subscribe((data : UserModel) => {
        console.log(data);
      });
    }
}